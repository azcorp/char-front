/**
 * Created by az on 19.03.17.
 */
export class Equip{
    constructor(){
        this.collection=[];
        this.price=0;
        this.weight=0;
    }
    id: string;
    name: string;
    price: number;
    weight: number;
    description: string;
    image_link: string;
    collection: any;
}
