import {Equip} from "./Equip";
/**
 * Created by az on 19.03.17.
 */
export class Pack{
    id: string;
    name: string;
    material: string;
    weight: number;
    price: number;
    size: string;
    description: string;
    image_link: string;
    enabled: boolean;
    equip: string;
    sale: boolean;
}