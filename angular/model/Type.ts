/**
 * Created by az on 19.03.17.
 */
export class Type {
    id:string;
    name:string;
    description:string;
    subtypes;
    image_link:string;

}