
import {Type} from "./Type";
/**
 * Created by az on 19.03.17.
 */
export class Sweet {
    id: string;
    name: string;
    image_link: string;
    price: number;
    weight: number;
    type: Type;
    subtype: string;
    enabled: boolean;
    factory: any;
    description: string;
}