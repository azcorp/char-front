/**
 * Created by zelenin on 02.11.16.
 */
import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {HomeComponent} from "./component/home.component";
import {CatalogComponent} from "./component/catalog.component";
import {DiscountsComponent} from "./component/discounts.component";
import {DeliveryComponent} from "./component/delivery.component";
import {SaleComponent} from "./component/sale.component";
import {ContactsComponent} from "./component/contacts.component";
import {BucketComponent} from "./component/bucket.component";

const routes: Routes =[
    {path: '',redirectTo: 'home',pathMatch: 'full'},
    {path:'home',component:HomeComponent},
    {path:'catalog',component:CatalogComponent},
    {path:'discounts',component:DiscountsComponent},
    {path:'delivery',component:DeliveryComponent},
    {path:'sale',component:SaleComponent},
    {path:'contacts',component:ContactsComponent},
    {path:'bucket',component:BucketComponent}
];
@NgModule({
    imports:[ RouterModule.forRoot(routes)],
    exports: [ RouterModule ],
})
export class RoutingModule{}