/**
 * Created by zelenin on 26.08.16.
 */
import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule} from "@angular/http";
import {FormsModule} from "@angular/forms";
import {RoutingModule} from "./routing.module";
import {OutputService} from "./service/output.service";
import {RootComponent} from "./component/root.component";
import {NavComponet} from "./component/nav.component";
import {OutputComponet} from "./component/output.component";
import {RootService} from "./service/root.service";
import {UploadFilesComponent} from "./component/upload-files.component";
import {InputModalComponent} from "./component/input-modal.component";

import {ImageModalComponent} from "./component/sweet-modal.component";
import {HomeComponent} from "./component/home.component";
import {CatalogComponent} from "./component/catalog.component";
import {SweetComponent} from "./component/sweet.component";
import {EquipComponent} from "./component/equip.comonent";
import {PackComponent} from "./component/pack.component";
import {FilterPipe} from "./pipe/filter.pipe";
import {DiscountsComponent} from "./component/discounts.component";
import {DeliveryComponent} from "./component/delivery.component";
import {SaleComponent} from "./component/sale.component";
import {ContactsComponent} from "./component/contacts.component";
import {BucketComponent} from "./component/bucket.component";
import {BucketService} from "./service/bucket.service";
import {PackModalComponent} from "./component/pack-modal.component";
import {AuthService} from "./service/auth.service";


@NgModule({
    imports:[
        BrowserModule,
        HttpModule,
        FormsModule,
        RoutingModule
    ],
    declarations: [
        RootComponent,
        NavComponet,
        OutputComponet,
        UploadFilesComponent,
        InputModalComponent,
        HomeComponent,
        ImageModalComponent,
        CatalogComponent,
        SweetComponent,
        EquipComponent,
        PackComponent,
        FilterPipe,
        DiscountsComponent,
        DeliveryComponent,
        SaleComponent,
        ContactsComponent,
        BucketComponent,
        PackComponent,
        PackModalComponent
    ],

    providers:    [
        OutputService,
        BucketService,
        AuthService
    ],
    bootstrap:    [ RootComponent ]
})
export class MainModule {}