/**
 * Created by zelenin on 10.01.17.
 */
import {Component, OnInit} from "@angular/core";
import {RootService} from "../service/root.service";


@Component({
    selector: 'home-component',
    templateUrl: '../templates/home.component.html'
})
export class HomeComponent implements OnInit {
    constructor(private rs:RootService) { }
    ngOnInit() {

    }
}