/**
 * Created by zelenin on 31.08.16.
 */
import {Component, OnInit} from "@angular/core";
import {OutputService} from "../service/output.service";
@Component({
    selector: 'output-component',
    template: `
        <div class="output-component">
            <div *ngFor="let item of output.items" class="callout {{item?.style}}">
                <div style="display: flex; justify-content: space-between">
                    <strong>{{item?.title}}</strong>
                    <button (click)="output.deleteItem(item)" class="close" type="button" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
               
                <div>{{item?.message}}</div>
            </div>
        </div>`
})

export class OutputComponet implements OnInit {
    constructor(public output: OutputService) {
    }

    ngOnInit(): void {

    }


}