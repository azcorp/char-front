/**
 * Created by zelenin on 28.08.16.
 */
import {Component, OnInit} from "@angular/core";
import {Router, NavigationEnd} from "@angular/router";
import {BucketService} from "../service/bucket.service";
import {AuthService} from "../service/auth.service";

@Component({
    selector: 'nav-component',
    templateUrl: '../templates/nav.component.html',
    styleUrls: ['../style/nav.component.scss']
})

export class NavComponet implements OnInit{
    constructor(private router:Router,private bs:BucketService,private auth:AuthService) {
       router.events.subscribe((event)=>{
          if(event instanceof NavigationEnd){
              switch (event.urlAfterRedirects) {
                  case "/home":
                      this.currentPosition="Главная";
                      this.currentIcon="fa fa-home";
                      break;
                  case "/catalog":
                      this.currentPosition="Каталог";
                      this.currentIcon="fa fa-bars";
                      break;
                  case "/discounts":
                      this.currentPosition="Скидки";
                      this.currentIcon="fa fa-ruble";
                      break;
                  case "/delivery":
                      this.currentPosition="Доставка";
                      this.currentIcon="fa fa-car";
                      break;
                  case "/sale":
                      this.currentPosition="Распродажа";
                      this.currentIcon="fa fa-shopping-cart";
                      break;
                  case "/contacts":
                      this.currentPosition="Контакты";
                      this.currentIcon="fa fa-map-marker";
                      break;
                  case "/bucket":
                      this.currentPosition="Корзина";
                      this.currentIcon="fa fa-shopping-basket";
                      break;
              }
          }
       })
    }
    currentPosition;
    currentIcon;
    ngOnInit(): void { }
    list=[
        {link:"/home",name:"Главная"},
        {link:"/catalog",name:"Каталог"},
        {link:"/discounts",name:"Скидки"},
        {link:"/delivery",name: "Доставка"},
        {link:"/sale",name:"Распродажа"},
        {link:"/contacts",name:"Контакты"}
    ];
    openForm(event){
        if(event.metaKey)
        this.auth.form=!this.auth.form;
    }


}