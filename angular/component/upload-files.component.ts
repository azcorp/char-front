/**
 * Created by zelenin on 17.11.16.
 */
import { Component, OnInit } from '@angular/core';
import {DavService} from "../service/dav.service";

@Component({
    selector: 'upload-files-component',
    template: `
<button class="dropdown-item button hollow  expanded uploadbutton"  (change)="selector($event)">
  <label class="custom-file uploadlabel">
      <input class="custom-file-input" id="uploader" type="file" name="files"  style="display: none;"/>
      <span class="custom-file-control uploader">
      <b style="color: #e6e6e6" class="uploadtext">Загрузить фото</b>
      <i style="color: #e6e6e6" *ngIf="davservice.preset!=null" class="fa fa-check" ></i>
      </span>
  </label>
</button>`,
    styleUrls: ['../style/upload-files.comonent.scss']
})
export class UploadFilesComponent implements OnInit {
    constructor(private davservice:DavService){}
    ngOnInit() { }
    selector(event){
        this.davservice.preset=event;
    }
}