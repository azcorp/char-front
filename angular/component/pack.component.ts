/**
 * Created by az on 24.03.17.
 */
import {Component, OnInit, Input} from '@angular/core';
import {Pack} from "../model/Pack";
import {CatalogService} from "../service/catalog.service";
import {DavService} from "../service/dav.service";

@Component({
    selector: 'pack-component',
    templateUrl: '../templates/pack.component.html',
    styleUrls: ['../style/pack.component.scss']
})
export class PackComponent implements OnInit {
    constructor(private catalogservice:CatalogService,private davservice:DavService) { }

    @Input() pack:Pack;

    ngOnInit() {
        console.log(this.pack);
    }
    openModal(event){
        console.log(event);
        if(event.metaKey){
            this.catalogservice.deletePack(this.pack.id).subscribe(
                data=>{
                    this.davservice.delete(this.pack.id);
                    this.catalogservice.updatePacklist();
                },
                error=>{
                    console.log(error);
                }
            )
        }else if(event.altKey){
            this.catalogservice.pack = this.pack;

        }else{
            this.catalogservice.packModal.pack = this.pack;
            this.catalogservice.packModal.visible = true;
        }
    }

}