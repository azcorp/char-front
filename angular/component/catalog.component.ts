/**
 * Created by az on 19.03.17.
 */
import {Component, OnInit} from '@angular/core';
import {CatalogService} from "../service/catalog.service";
import {Sweet} from "../model/Sweet";
import {error} from "util";
import {DavService} from "../service/dav.service";
import {Pack} from "../model/Pack";
import {Equip} from "../model/Equip";
import {Type} from "../model/Type";
import {Packtype} from "../model/Packtype";
import {Factory} from "../model/Factory";
import {el} from "@angular/platform-browser/testing/browser_util";
import * as _ from 'lodash';
import {FilterPipe} from "../pipe/filter.pipe";
import {AuthService} from "../service/auth.service";
@Component({
    selector: 'catalog-component',
    templateUrl: '../templates/catalog.component.html',
    styleUrls: ['../style/catalog.component.scss']
})
export class CatalogComponent implements OnInit {
    constructor(private catalogservice: CatalogService, private davservice: DavService,private auth:AuthService) {
        this.catalogservice.refreshHeaders();
        this.auth.refreshHeaders();
    }

    entity = "pack";
    creator = false;
    digest = "type";
    filterMain: string;
    subtypesline="";

    ngOnInit() {

    }

    updateOrAddSweet(sweet) {
        if (this.catalogservice.sweet.id == null) {
            this.addSweet(sweet);
        } else {
            this.updateSweet(sweet);
        }
    }
    addSweet(sweet) {
        this.catalogservice.sweet.id = null;
        this.catalogservice.postSweet(sweet).subscribe(
            data => {
                if (this.davservice.preset != null) {
                    this.davservice.put(data.id).subscribe(
                        link => {
                            sweet.image_link = link;
                            sweet.id = data.id;
                            console.log(sweet);
                            this.catalogservice.updateSweet(sweet).subscribe(
                                data => {
                                    sweet.image_link = null;
                                    sweet.id = null;
                                    this.catalogservice.updateSweetlist();
                                    console.log(data);
                                },
                                error => {
                                    console.log(error);
                                }
                            )
                        }
                    )
                } else {
                    this.catalogservice.updateSweetlist();
                }

            },
            error => console.log(error)
        );
    }
    updateSweet(sweet) {
        if (this.catalogservice.sweet.id != null) {
            if(this.davservice.preset!=null){
                this.davservice.put(this.catalogservice.sweet.id).subscribe(
                    link => {
                        console.log(link);
                        sweet.image_link = link;
                        console.log(sweet);
                        this.catalogservice.updateSweet(sweet).subscribe(
                            data => {
                                console.log(data);
                                this.catalogservice.updateSweetlist();
                                this.catalogservice.sweet = new Sweet();
                            },
                            error => {
                                console.log(error);
                            }
                        )
                    }
                )
            }else {
                this.catalogservice.updateSweet(sweet).subscribe(
                    data => {
                        console.log(data);
                        this.catalogservice.updateSweetlist();
                        this.catalogservice.sweet = new Sweet();
                    },
                    error => {
                        console.log(error);
                    })
            }
        }
    }

    updateOrAddPack(pack) {
        if (this.catalogservice.pack.id == null) {
            this.addPack(pack);
        } else {
            this.updatePack(pack);
        }
    }
    addPack(pack) {
        this.catalogservice.pack.id = null;
        this.catalogservice.postPack(pack).subscribe(
            data => {
                if (this.davservice.preset != null) {
                    this.davservice.put(data.id).subscribe(
                        link => {
                            pack.image_link = link;
                            pack.id = data.id;
                            console.log(pack);
                            this.catalogservice.updatePack(pack).subscribe(
                                data => {
                                    pack.image_link = null;
                                    pack.id = null;
                                    this.catalogservice.updatePacklist();
                                    console.log(data);
                                },
                                error => {
                                    console.log(error);
                                }
                            )
                        }
                    )
                } else {
                    this.catalogservice.updatePacklist();
                }
            },
            error => console.log(error)
        );
    }
    updatePack(pack) {
        if (this.catalogservice.pack.id != null) {
            if(this.davservice.preset!=null){
                this.davservice.put(this.catalogservice.pack.id).subscribe(
                    link => {
                        console.log(link);
                        pack.image_link = link;
                        console.log(pack);
                        this.catalogservice.updatePack(pack).subscribe(
                            data => {
                                console.log(data);
                                this.catalogservice.updatePacklist();
                                this.catalogservice.pack = new Pack();
                            },
                            error => {
                                console.log(error);
                            }
                        )
                    }
                )
            }else {
                this.catalogservice.updatePack(pack).subscribe(
                    data => {
                        console.log(data);
                        this.catalogservice.updatePacklist();
                        this.catalogservice.pack = new Pack();
                    },
                    error => {
                        console.log(error);
                    })
            }
        }
    }

    updateOrAddEquip(equip){
        if (this.catalogservice.equip.id == null) {
            this.addEquip(equip);
        } else {
            this.updateEquip(equip);
        }
    }
    addEquip(equip) {
        this.catalogservice.equip.id=null;
        this.catalogservice.postEquip(equip).subscribe(
            data => {
                this.davservice.put(data.id).subscribe(
                    link => {
                        equip.image_link = link;
                        equip.id = data.id;
                        console.log(equip);
                        this.catalogservice.updateEquip(equip).subscribe(
                            data => {
                                this.catalogservice.updateEquiplist();
                                console.log(data);
                            },
                            error => {
                                console.log(error);
                            }
                        )
                    }
                )
            },
            error => console.log(error)
        );
    }
    updateEquip(equip){
        if (this.catalogservice.equip.id != null) {
            if(this.davservice.preset!=null){
                this.davservice.put(this.catalogservice.equip.id).subscribe(
                    link => {
                        console.log(link);
                        equip.image_link = link;
                        console.log(equip);
                        this.catalogservice.updateEquip(equip).subscribe(
                            data => {
                                console.log(data);
                                this.catalogservice.updateEquiplist();
                                this.catalogservice.equip = new Equip();
                            },
                            error => {
                                console.log(error);
                            }
                        )
                    }
                )
            }else {
                this.catalogservice.updateEquip(equip).subscribe(
                    data => {
                        console.log(data);
                        this.catalogservice.updateEquiplist();
                        this.catalogservice.equip = new Equip();
                    },
                    error => {
                        console.log(error);
                    })
            }
        }
    }

    addSweetToEquipCollection(sweet) {
        let index = _.findIndex(this.catalogservice.equip.collection,(o)=>{ return o['id']==sweet.id});
        if (index == -1) {
            this.catalogservice.equip.collection.push({quantity: 1, id: sweet.id});

        } else {
            this.catalogservice.equip.collection[index].quantity++;
        }
        this.recalcEquip();
    }

    deleteSweetFromEquipCollection(id) {
        _.remove(this.catalogservice.equip.collection,(o)=>{
          return o['id']== id;
        });
        this.recalcEquip();
    }

    recalcEquip(){
        this.catalogservice.equip.price=0;
        this.catalogservice.equip.weight=0;
        this.catalogservice.equip.collection.forEach((pair)=>{
            console.log(pair);
            let sweet = this.catalogservice.getSweetById(pair.id);
           this.catalogservice.equip.price+=sweet.price*pair.quantity;
            this.catalogservice.equip.weight+=sweet.weight*pair.quantity;
        })
    }

    splitAndSetSutypes(){

        this.catalogservice.type.subtypes = this.subtypesline.split("\n");
        console.log(this.catalogservice.type.subtypes);
    }
    joinAndSetSubtypeslin(){
        if(this.catalogservice.type.subtypes!==undefined){
            this.subtypesline = this.catalogservice.type.subtypes.join("\n");
            console.log(this.subtypesline);
        }
    }

    alterType(type,event){
        if(event.altKey){
            this.catalogservice.type=type;
            this.joinAndSetSubtypeslin();
        }else if(event.metaKey){
         this.catalogservice.deleteType(type.id).subscribe(
             data=>{
                 this.catalogservice.updateTypelist();
             },
             error=>{}
         );

        }

    }
    updateOrAddType(type){
        if (this.catalogservice.type.id == null) {
            this.addType(type);
        } else {
            this.updateType(type);
        }
    }
    addType(type){
        this.catalogservice.type.id = null;
        this.catalogservice.postType(type).subscribe(
            data => {
                if (this.davservice.preset != null) {
                    this.davservice.put(data.id).subscribe(
                        link => {
                            type.image_link = link;
                            type.id = data.id;
                            console.log(type);
                            this.catalogservice.updateType(type).subscribe(
                                data => {
                                    type.image_link = null;
                                    type.id = null;
                                    this.catalogservice.updateTypelist();
                                    console.log(data);
                                },
                                error => {
                                    console.log(error);
                                }
                            )
                        }
                    )
                } else {
                    this.catalogservice.updateTypelist();
                }

            },
            error => console.log(error)
        );
    }
    updateType(type){
        if (this.catalogservice.type.id != null) {
            if(this.davservice.preset!=null){
                this.davservice.put(this.catalogservice.factory.id).subscribe(
                    link => {
                        console.log(link);
                        type.image_link = link;
                        console.log(type);
                        this.catalogservice.updateType(type).subscribe(
                            data => {
                                console.log(data);
                                this.catalogservice.updateTypelist();
                                this.catalogservice.type = new Type;
                                this.subtypesline ="";
                            },
                            error => {
                                console.log(error);
                            }
                        )
                    }
                )
            }else {
                this.catalogservice.updateType(type).subscribe(
                    data => {
                        console.log(data);
                        this.catalogservice.updateTypelist();
                        this.catalogservice.type = new Type;
                        this.subtypesline ="";
                    },
                    error => {
                        console.log(error);
                    })
            }
        }
    }

    alterFactory(factory,event){
        if(event.altKey){
            this.catalogservice.factory=factory;
        } else if(event.metaKey){
            this.catalogservice.deleteFactory(factory.id).subscribe(
                data=>{
                    this.catalogservice.updateFactorylist();
                },
                error=>{}
            );

        }

    }
    updateOrAddFactory(factory){
        if (this.catalogservice.factory.id == null) {
            this.addFactory(factory);
        } else {
            this.updateFactory(factory);
        }
    }
    addFactory(factory) {
        this.catalogservice.factory.id = null;
        this.catalogservice.postFactory(factory).subscribe(
            data => {
                if (this.davservice.preset != null) {
                    this.davservice.put(data.id).subscribe(
                        link => {
                            factory.image_link = link;
                            factory.id = data.id;
                            console.log(factory);
                            this.catalogservice.updateFactory(factory).subscribe(
                                data => {
                                    factory.image_link = null;
                                    factory.id = null;
                                    this.catalogservice.updateFactorylist();
                                    console.log(data);
                                },
                                error => {
                                    console.log(error);
                                }
                            )
                        }
                    )
                } else {
                    this.catalogservice.updateFactorylist();
                }

            },
            error => console.log(error)
        );
    }
    updateFactory(factory){
        if (this.catalogservice.factory.id != null) {
            if(this.davservice.preset!=null){
                this.davservice.put(this.catalogservice.factory.id).subscribe(
                    link => {
                        console.log(link);
                        factory.image_link = link;
                        console.log(factory);
                        this.catalogservice.updateFactory(factory).subscribe(
                            data => {
                                console.log(data);
                                this.catalogservice.updateFactorylist();
                                this.catalogservice.factory = new Factory();
                            },
                            error => {
                                console.log(error);
                            }
                        )
                    }
                )
            }else {
                this.catalogservice.updateFactory(factory).subscribe(
                    data => {
                        console.log(data);
                        this.catalogservice.updateFactorylist();
                        this.catalogservice.factory = new Factory();
                    },
                    error => {
                        console.log(error);
                    })
            }
        }
    }

    alterPacktype(packtype,event){
        if(event.altKey){
            this.catalogservice.packtype=packtype;
        }else if(event.metaKey){
            console.log(event);
            console.log(packtype);
            this.catalogservice.deletePacktype(packtype.id).subscribe(
                data=>{
                    this.catalogservice.updatePacktypelist();
                },
                error=>{}
            );

        }


    }
    updateOrAddPacktype(packtype){
        if (this.catalogservice.packtype.id == null) {
            this.addPacktype(packtype);
        } else {
            this.updatePacktype(packtype);
        }
    }
    addPacktype(packtype) {
        this.catalogservice.packtype.id = null;
        this.catalogservice.postPacktype(packtype).subscribe(
            data => {
                if (this.davservice.preset != null) {
                    this.davservice.put(data.id).subscribe(
                        link => {
                            packtype.image_link = link;
                            packtype.id = data.id;
                            console.log(packtype);
                            this.catalogservice.updatePacktype(packtype).subscribe(
                                data => {
                                    packtype.image_link = null;
                                    packtype.id = null;
                                    this.catalogservice.updatePacktypelist();
                                    console.log(data);
                                },
                                error => {
                                    console.log(error);
                                }
                            )
                        }
                    )
                } else {
                    this.catalogservice.updatePacktypelist();
                }

            },
            error => console.log(error)
        );
    }
    updatePacktype(packtype){
        if (this.catalogservice.packtype.id != null) {
            if(this.davservice.preset!=null){
                this.davservice.put(this.catalogservice.packtype.id).subscribe(
                    link => {
                        console.log(link);
                        packtype.image_link = link;
                        console.log(packtype);
                        this.catalogservice.updatePacktype(packtype).subscribe(
                            data => {
                                console.log(data);
                                this.catalogservice.updatePacktypelist();
                                this.catalogservice.packtype = new Packtype();
                            },
                            error => {
                                console.log(error);
                            }
                        )
                    }
                )
            }else {
                this.catalogservice.updatePacktype(packtype).subscribe(
                    data => {
                        console.log(data);
                        this.catalogservice.updatePacktypelist();
                        this.catalogservice.packtype = new Packtype();
                    },
                    error => {
                        console.log(error);
                    })
            }
        }
    }


}