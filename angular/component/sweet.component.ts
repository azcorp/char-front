/**
 * Created by az on 20.03.17.
 */
import {Component, OnInit, Input} from '@angular/core';
import {Sweet} from "../model/Sweet";
import {CatalogService} from "../service/catalog.service";
import {DavService} from "../service/dav.service";
import {BucketService} from "../service/bucket.service";

@Component({
    selector: 'sweet-comonent',
    templateUrl: '../templates/sweet.component.html',
    styleUrls: ['../style/sweet.component.scss']
})
export class SweetComponent implements OnInit {
    constructor(private catalogservice:CatalogService,private davservice:DavService,private bucket:BucketService) { }
    @Input() sweet:Sweet;
    @Input() view="full";

    ngOnInit() { }
    openModal(event){
        console.log(event);
        if(event.metaKey){
            this.catalogservice.deleteSweet(this.sweet.id).subscribe(
                data=>{
                    this.davservice.delete(this.sweet.id);
                    this.catalogservice.updateSweetlist();
                },
                error=>{
                    console.log(error);
                }
            )
        }else if(event.altKey){
            this.catalogservice.setSubtypes(this.sweet.type);
            this.catalogservice.sweet = this.sweet;

        }else{
            this.catalogservice.sweetModal.sweet = this.sweet;
            this.catalogservice.sweetModal.visible = true;
        }
    }
    addToBucket(sweet){
        this.bucket.addToBucket(sweet);
    }
}