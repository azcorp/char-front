/**
 * Created by zelenin on 14.01.17.
 */
import {Component, OnInit, Input} from '@angular/core';
import * as _ from 'lodash';
import {DavService} from "../service/dav.service";
import {CatalogService} from "../service/catalog.service";
import {BucketService} from "../service/bucket.service";

@Component({
    selector: 'sweet-modal-component',
    templateUrl: '../templates/sweet-modal.component.html',
    styleUrls: ['../style/modal.component.scss']
})
export class ImageModalComponent implements OnInit {
    constructor(private catalogserice:CatalogService,private davservice:DavService,private bucket:BucketService) { }
    ngOnInit() {
      this.factory = this.catalogserice.getFactoryById(this.sweetModal.sweet.factory);
      this.type = this.catalogserice.getTypeById(this.sweetModal.sweet.type)
    }
    factory;
    type;
    quantity=1;
    @Input() sweetModal;
    close(){
        this.sweetModal.visible=false
    }
    addToBucket(sweet){
        this.bucket.addToBucketMulti({quantity:this.quantity,obj:sweet});
    }






}