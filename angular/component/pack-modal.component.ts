/**
 * Created by zelenin on 14.01.17.
 */
import {Component, OnInit, Input, OnChanges, SimpleChanges, AfterViewInit} from '@angular/core';
import * as _ from 'lodash';
import {DavService} from "../service/dav.service";
import {CatalogService} from "../service/catalog.service";
import {BucketService} from "../service/bucket.service";
import {OutputService} from "../service/output.service";

@Component({
    selector: 'pack-modal-component',
    templateUrl: '../templates/pack-modal.component.html',
    styleUrls: ['../style/modal.component.scss']
})
export class PackModalComponent implements OnInit, AfterViewInit {
    constructor(private catalogservice: CatalogService,
                private davservice: DavService,
                private bucket: BucketService,
                private output:OutputService) {
    }

    ngOnInit() {

    }

    ngAfterViewInit() {
        this.price = this.packModal.pack.price;
        this.weight = this.packModal.pack.weight;
        this.setEquip();
    }

    quantity = 1;
    equipid="empty";
    price;
    weight;

    @Input() packModal;

    close() {
        this.packModal.visible = false;
        this.packModal.pack.price = this.price;
        this.packModal.pack.weight = this.weight;
    }

    setEquip() {
        this.packModal.pack.price = this.price;
        this.packModal.pack.weight = this.weight;
        this.packModal.pack.equip = this.catalogservice.getEquipById(this.equipid);
        if(this.packModal.pack.equip!=null){
            this.packModal.pack.price = this.price + this.packModal.pack.equip.price;
            this.packModal.pack.weight = this.weight + this.packModal.pack.equip.weight;
        }

    }

    addToBucket(pack) {
        let obj = _.clone(this.packModal.pack);
        if(this.packModal.pack.equip!=null){
            obj.weight = this.weight + this.packModal.pack.equip.weight;
            obj.price = this.price + this.packModal.pack.equip.price;
        }

        this.bucket.addToBucketMulti({quantity: this.quantity, obj: obj});
        this.output.addItem({title:"Корзина",message: this.quantity+" Добавленно в корзину",style:"success"})
    }


}