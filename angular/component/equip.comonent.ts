/**
 * Created by az on 24.03.17.
 */
import {Component, OnInit, Input} from '@angular/core';
import {Equip} from "../model/Equip";
import {CatalogService} from "../service/catalog.service";
import {DavService} from "../service/dav.service";

@Component({
    selector: 'equip-component',
    templateUrl: '../templates/equip.component.html',
    styleUrls: ['../style/equip.component.scss']
})
export class EquipComponent implements OnInit {
    constructor(private catalogservice:CatalogService,private  davservice:DavService) { }

    ngOnInit() {
        new Promise((resolve)=>{
            let counter = setInterval(()=>{
                console.log("wait for sweetlist");
                if(this.catalogservice.downloadState.sweetlist==true){
                    resolve(true);
                    clearInterval(counter);
                }
            },1);
        }).then(()=>{
            this.equip.collection.forEach(item=>{
                this.collection.push({quantity:item.quantity,sweet:this.catalogservice.getSweetById(item.id)})
            })
        });
    }
    @Input() equip:Equip;
    @Input() view='full';
    collection=[];
    openModal(event){
        console.log(event);
        if(event.metaKey){
            this.catalogservice.deleteEquip(this.equip.id).subscribe(
                data=>{
                    this.davservice.delete(this.equip.id);
                    this.catalogservice.updateEquiplist();
                },
                error=>{
                    console.log(error);
                }
            )
        }else if(event.altKey){
            this.catalogservice.equip = this.equip;

        }else{

        }
    }
}