/**
 * Created by az on 28.03.17.
 */
import { Component, OnInit } from '@angular/core';
import {BucketService} from "../service/bucket.service";
import {AuthService} from "../service/auth.service";
import {CatalogService} from "../service/catalog.service";

@Component({
    selector: 'bucket-component',
    templateUrl: '../templates/bucket.component.html'
})
export class BucketComponent implements OnInit {
    constructor(private bs:BucketService,private auth:AuthService,private catalogservice:CatalogService) { }
    ngOnInit() { }
    removeFromBucket(sweet){
        this.bs.removeFromBucket(sweet);
    }
}