/**
 * Created by zelenin on 26.08.16.
 */
import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {DavService} from "../service/dav.service";
import {RootService} from "../service/root.service";
import {CatalogService} from "../service/catalog.service";
require('../style/root.comonent.scss');
@Component({
    selector: 'root-component',
    template: `
        <output-component>
            <i class="fa fa-spinner fa-cog fa-5x fa-fw"></i><span class="sr-only">Loading...</span>
        </output-component>
        <div class="root-component">

            <nav-component>
                <i class="fa fa-spinner fa-cog fa-5x fa-fw"></i><span class="sr-only">Loading...</span>
            </nav-component>

            <div class="outlet">
                <router-outlet></router-outlet>
            </div>
        </div>`,
    styleUrls: ['../style/root.comonent.scss'],
    providers: [RootService, DavService, CatalogService],
    encapsulation: ViewEncapsulation.None,
})
export class RootComponent implements OnInit {
    constructor(private cs: CatalogService) {

    }

    ngOnInit(): void {
        this.cs.updateSweetlist();
        this.cs.updateEquiplist();
        this.cs.updateTypelist();
        this.cs.updatePacklist();
        this.cs.updatePacktypelist();
        this.cs.updateFactorylist();
    }


}
