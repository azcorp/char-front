/**
 * Created by az on 28.03.17.
 */
import { Component, OnInit } from '@angular/core';
import {CatalogService} from "../service/catalog.service";

@Component({
    selector: 'sale-component',
    templateUrl: '../templates/sale.component.html'
})
export class SaleComponent implements OnInit {
    constructor(private  catalogservice:CatalogService) { }

    ngOnInit() { }

}