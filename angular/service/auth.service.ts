/**
 * Created by az on 01.04.17.
 */
import {Inject, Injectable} from "@angular/core";
import "rxjs/add/operator/catch";
import "rxjs/add/operator/map";
import {Http, RequestOptions,Response,Headers} from "@angular/http";
import {Observable} from "rxjs";

@Injectable()
export class AuthService {
    constructor(private http:Http) {
      this.token = localStorage.getItem("token");
        console.log(localStorage.getItem("token"));

    }
    headers = new Headers({
        'Content-Type': 'application/json',
        'Accept': 'application/json;q=0.9,*/*;q=0.8',
        'Authorization': localStorage.getItem("token")
    });
    refreshHeaders(){
        this.headers = new Headers({
            'Content-Type': 'application/json',
            'Accept': 'application/json;q=0.9,*/*;q=0.8',
            'Authorization': localStorage.getItem("token")
        });
        this.options = new RequestOptions({ headers: this.headers });
    }

    options = new RequestOptions({ headers: this.headers });
    loginurl=sessionStorage.getItem("protocol")+"://"+sessionStorage.getItem("server")+"/api/users/login";
    logouturl=sessionStorage.getItem("protocol")+"://"+sessionStorage.getItem("server")+"/api/users/logout";
    token;
    form=false;

    email="";
    password="";

    login(){
        this.http.post(this.loginurl,JSON.stringify({email:this.email,password:this.password}),this.options)
            .map(this.extractData)
            .catch(this.handleError).subscribe(
                data=>{
                    this.token=data.id;
                    localStorage.setItem("token",this.token)

                },
            error=>{}
        )
    }
    logout(){
        this.http.post(this.logouturl,JSON.stringify({email:this.email,password:this.password}),this.options)
            .map(this.extractData)
            .catch(this.handleError).subscribe(
            data=>{
                this.token=null;
                localStorage.removeItem("token")
            },
            error=>{}
        )
    }


    private extractData(res: Response) {
        let body = res.json();
        return body;
    }

    private handleError (error: Response | any) {
        // In a real world app, you might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}