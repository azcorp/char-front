/**
 * Created by zelenin on 03.10.16.
 */
import {Injectable} from "@angular/core";
import {Headers} from "@angular/http";


@Injectable()
export class RootService{
    constructor() {
        this.headers = new Headers({
            'Content-Type': 'application/json',
            'Accept': 'application/json;q=0.9,*/*;q=0.8',
            'Authorization':'Bearer '+localStorage.getItem("token")
        });
    }
    public headers;
    public profile=JSON.parse(sessionStorage.getItem("profile"));


}

