/**
 * Created by az on 19.03.17.
 */
import { Injectable }              from '@angular/core';
import {Http, Response, Headers, RequestOptions}          from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Sweet } from '../model/Sweet';
import {Equip} from "../model/Equip";
import {Pack} from "../model/Pack";
import {Type} from "../model/Type";
import {Packtype} from "../model/Packtype";
import {Factory} from "../model/Factory";
import * as _ from 'lodash';

@Injectable()
export class CatalogService {
    constructor (private http: Http) {

    }
    headers = new Headers({
    'Content-Type': 'application/json',
    'Accept': 'application/json;q=0.9,*/*;q=0.8',
    'Authorization': localStorage.getItem("token")
    });
    options = new RequestOptions({ headers: this.headers });

    private sweets = sessionStorage.getItem("protocol")+"://"+sessionStorage.getItem("server")+"/api/sweets/";
    private equips = sessionStorage.getItem("protocol")+"://"+sessionStorage.getItem("server")+"/api/equips/";
    private packs = sessionStorage.getItem("protocol")+"://"+sessionStorage.getItem("server")+"/api/packs/";
    private types = sessionStorage.getItem("protocol")+"://"+sessionStorage.getItem("server")+"/api/types/";
    private packtypes = sessionStorage.getItem("protocol")+"://"+sessionStorage.getItem("server")+"/api/packtypes/";
    private factories = sessionStorage.getItem("protocol")+"://"+sessionStorage.getItem("server")+"/api/factories/";

    refreshHeaders(){
        this.headers = new Headers({
            'Content-Type': 'application/json',
            'Accept': 'application/json;q=0.9,*/*;q=0.8',
            'Authorization': localStorage.getItem("token")
        });
        this.options = new RequestOptions({ headers: this.headers });
    }

    downloadState={
        sweetlist:false,
        packlist:false,
        equiplist:false,
        typelist:false,
        packtypelist:false,
        factorylist:false
    };

    sweetModal={visible:false,title:"Viewer",sweet:null};
    packModal={visible:false,title:"Viewer",pack:null};

    sweetlist=[];
    packlist=[];
    equiplist=[];
    typelist=[];
    packtypelist=[];
    factorylist=[];
    subtypelist=[];

    sweet = new Sweet();
    equip = new Equip;
    pack = new Pack();
    type = new Type();
    packtype =new Packtype();
    factory = new Factory();

    updateSweetlist(){
        this.getSweets().subscribe(
            data => {
                this.sweetlist = data;
                this.downloadState.sweetlist=true;
            },
            error => console.log(error)
        )
    }
    updateEquiplist(){
        this.getEquips().subscribe(
            data => {
                this.equiplist = data;
                this.downloadState.equiplist=true;
            },
            error => console.log(error)
        )
    }
    updatePacklist(){
        this.getPacks().subscribe(
            data => {
                this.packlist = data;
                this.downloadState.packlist=true;
            },
            error => console.log(error)
        )
    }
    updateTypelist(){
        this.getTypes().subscribe(
            data => {
                this.typelist = data;
                this.downloadState.typelist=true;
            },
            error => console.log(error)
        )
    }
    updatePacktypelist(){
        this.getPacktypes().subscribe(
            data => {
                this.packtypelist = data;
                this.downloadState.packtypelist=true;
            },
            error => console.log(error)
        )
    }
    updateFactorylist(){
        this.getFactories().subscribe(
            data => {
                this.factorylist = data;
                this.downloadState.factorylist=true;
            },
            error => console.log(error)
        )
    }


    setSubtypes(id){
        console.log(id);
        this.subtypelist= _.find(this.typelist,(type)=>{
            return id == type.id;
        }).subtypes;
    }
    getSweetById(id){
        return _.find(this.sweetlist,(o)=>{
            return o.id==id
        });
    }
    getSweetIndexById(id){
        return _.findIndex(this.sweetlist,(o)=>{
            return o.id==id
        })
    }

    getFactoryById(id){
        return _.find(this.factorylist,(o)=>{
            return o.id==id;
        })
    }
    getTypeById(id){
        return _.find(this.typelist,(o)=>{
            return o.id==id;
        });
    }
    getPacktypeById(id){
        return _.find(this.packtypelist,(o)=>{
            return o.id==id;
        });
    }
    getEquipById(id){
        return _.find(this.equiplist,(o)=>{
            return o.id==id;
        });
    }


    //Sweets
    getSweets(): Observable<Sweet[]> {
        return this.http.get(this.sweets,this.options).map(this.extractData).catch(this.handleError);
    }
    postSweet(sweet:Sweet):Observable<Sweet>{
        return this.http.post(this.sweets,JSON.stringify(sweet),this.options).map(this.extractData).catch(this.handleError)
    }
    deleteSweet(id):Observable <any>{
        return this.http.delete(this.sweets+id,this.options).map(this.extractData).catch(this.handleError)
    }
    updateSweet(sweet:Sweet):Observable<Sweet>{
        return this.http.patch(this.sweets+sweet.id,sweet,this.options).map(this.extractData).catch(this.handleError)
    }

    // Equips
    getEquips(): Observable<Equip[]> {
        return this.http.get(this.equips,this.options).map(this.extractData).catch(this.handleError);
    }
    postEquip(equip:Equip):Observable<Equip>{
        return this.http.post(this.equips,JSON.stringify(equip),this.options).map(this.extractData).catch(this.handleError)
    }
    deleteEquip(id):Observable <any>{
        return this.http.delete(this.equips+id,this.options).map(this.extractData).catch(this.handleError)
    }
    updateEquip(equip:Equip):Observable<Equip>{
        return this.http.patch(this.equips+equip.id,equip,this.options).map(this.extractData).catch(this.handleError)
    }

    // Packs
    getPacks(): Observable<Pack[]> {
        return this.http.get(this.packs,this.options).map(this.extractData).catch(this.handleError);
    }
    postPack(pack:Pack):Observable<Pack>{
        return this.http.post(this.packs,JSON.stringify(pack),this.options).map(this.extractData).catch(this.handleError)
    }
    deletePack(id):Observable <any>{
        return this.http.delete(this.packs+id,this.options).map(this.extractData).catch(this.handleError)
    }
    updatePack(pack:Pack):Observable<Pack>{
        return this.http.patch(this.packs+pack.id,pack,this.options).map(this.extractData).catch(this.handleError)
    }

    // Types
    getTypes(): Observable<Type[]> {
        return this.http.get(this.types,this.options).map(this.extractData).catch(this.handleError);
    }
    postType(type:Type):Observable<Type>{
        return this.http.post(this.types,JSON.stringify(type),this.options).map(this.extractData).catch(this.handleError)
    }
    deleteType(id):Observable <any>{
        return this.http.delete(this.types+id,this.options).map(this.extractData).catch(this.handleError)
    }
    updateType(type:Type):Observable<Type>{
        return this.http.patch(this.types+type.id,type,this.options).map(this.extractData).catch(this.handleError)
    }

    //Packtypes
    getPacktypes(): Observable<Packtype[]> {
        return this.http.get(this.packtypes,this.options).map(this.extractData).catch(this.handleError);
    }
    postPacktype(subtype:Packtype):Observable<Packtype>{
        return this.http.post(this.packtypes,JSON.stringify(subtype),this.options).map(this.extractData).catch(this.handleError)
    }
    deletePacktype(id):Observable <any>{
        return this.http.delete(this.packtypes+id,this.options).map(this.extractData).catch(this.handleError)
    }
    updatePacktype(packtype:Packtype):Observable<Packtype>{
        return this.http.patch(this.packtypes+packtype.id,packtype,this.options).map(this.extractData).catch(this.handleError)
    }

    //Factory
    getFactories(): Observable<Factory[]> {
        return this.http.get(this.factories,this.options).map(this.extractData).catch(this.handleError);
    }
    postFactory(factory:Factory):Observable<Factory>{
        return this.http.post(this.factories,JSON.stringify(factory),this.options).map(this.extractData).catch(this.handleError)
    }
    deleteFactory(id):Observable <any>{
        return this.http.delete(this.factories+id,this.options).map(this.extractData).catch(this.handleError)
    }
    updateFactory(factory:Factory):Observable<Factory>{
        return this.http.patch(this.factories+factory.id,factory,this.options).map(this.extractData).catch(this.handleError)
    }


    private extractData(res: Response) {
        let body = res.json();
        return body;
    }

    private handleError (error: Response | any) {
        // In a real world app, you might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}