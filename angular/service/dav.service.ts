/**
 * Created by zelenin on 07.09.16.
 */
import {Injectable, Inject} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import {Headers, RequestOptions, Http} from "@angular/http";
import {Observable} from 'rxjs/Observable';


@Injectable()
export class DavService {
    constructor() {}
    private url = `${sessionStorage.getItem("protocol")}://${sessionStorage.getItem("server")}/storage/`;
    preset;

    put(id): Observable<any> {
        if (this.preset.target.files.length > 0) {
            console.log(this.preset);
            return new Observable(
                observer => {
                    let abslouteurl = this.url + id + "/" + this.preset.target.files[0].name;
                    let relativeurl = id + "/" + this.preset.target.files[0].name;
                    let fileReader = new FileReader();
                    fileReader.readAsBinaryString(this.preset.target.files[0]);
                    fileReader.onload = () => {
                        let xhr: XMLHttpRequest = new XMLHttpRequest();
                        xhr.onreadystatechange = () => {
                            if (xhr.readyState === 4) {
                                if (xhr.status === 201 || xhr.status === 204) {
                                    observer.next(relativeurl);
                                    observer.complete();
                                    this.preset = null;
                                } else {
                                    console.log(xhr);
                                }
                            }
                        };
                        xhr.open('PUT', abslouteurl, true);
                        xhr.setRequestHeader("Content-Type", "application/octet-stream");
                        xhr.send(this.preset.target.files[0])
                    }
                }
            )

        }
    }

    delete(id) {
        console.log(this.url + id);
        let xhr: XMLHttpRequest = new XMLHttpRequest();
        xhr.open('DELETE', this.url + id + "/", true);
        xhr.send();
    }


}
