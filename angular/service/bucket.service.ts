/**
 * Created by az on 20.03.17.
 */
/**
 * Created by az on 19.03.17.
 */
import { Injectable }              from '@angular/core';
import {Http, Response, Headers, RequestOptions}          from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Sweet } from '../model/Sweet';
import {Bucket} from "../model/Bucket";
import {Pack} from "../model/Pack";
import * as _ from 'lodash';
@Injectable()
export class BucketService {
    constructor () {
        if(localStorage.getItem("bucket")!= null){
            this.pullBucketFromStorage()
        }
    }
    bucket=new Bucket();

    pushBucketToStorage(){
        localStorage.setItem("bucket",JSON.stringify(this.bucket))
    }
    pullBucketFromStorage(){
        this.bucket=JSON.parse(localStorage.getItem("bucket"));
    }

    clearBucket(){
        this.bucket = new Bucket();
        this.recalcBucket();
    }
    recalcBucket(){
        if(this.bucket.collection.length>0){
            this.bucket.sum=0;
            this.bucket.weight=0;
            this.bucket.collection.forEach((item)=>{
                if(item.obj.price!==undefined){
                    this.bucket.sum+=item.quantity*item.obj.price;
                }
                if(item.obj.weight!==undefined)
                this.bucket.weight+=item.quantity*item.obj.weight;
            })

        }else {
            this.bucket.sum=0;
            this.bucket.weight=0;
        }
        this.pushBucketToStorage();
    }

    addToBucket(obj){
        let index = _.findIndex(this.bucket.collection,(o)=>{
            return _.eq(o,obj)}
            );
        if (index == -1) {
            this.bucket.collection.push({quantity: 1, obj: obj})
        } else {
            this.bucket.collection[index].quantity++;
        }
        this.recalcBucket();
    }

    addToBucketMulti(pair){
        let index = _.findIndex(this.bucket.collection,(o)=>{
            return _.eq(o,pair)
        });
        if (index == -1) {
            this.bucket.collection.push({quantity:pair.quantity,obj:pair.obj})
        } else {
            this.bucket.collection[index].quantity+=pair.quantity;
        }
        this.recalcBucket();
        console.log(this.bucket);
    }

    removeFromBucket(obj){
        _.remove(this.bucket.collection,(o)=>{return _.eq(o,obj)});
        this.recalcBucket();
    }






}